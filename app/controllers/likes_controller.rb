class LikesController < ApplicationController

  def like_post
    @user = current_user
    @post = Post.find(params[:id])
    Like.create(user_id: @user.id, post_id: @post.id)
    redirect_to post_path(@post.id)
  end

  def unlike_post
    @user = current_user
    @post = Post.find(params[:id])
    @like = Like.where(user_id: @user.id, post_id: @post.id).first
    @like.destroy
    redirect_to post_path(@post.id)
  end

end
