class PostsController < ApplicationController
  def index
    @user = current_user
    @post = Post.new
  end

  def upload_post
    @user = current_user
    @post = @user.posts.create(post_params)
    redirect_to post_path(@post.id)
  end

  def view
    @post = Post.find(params[:id])
    @comment = Comment.new
  end

  private

  def post_params
    params.require(:post).permit(:image)
  end
end
