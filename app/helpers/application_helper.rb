module ApplicationHelper

  def likes(user, post)
    array = []
    @likes = post.likes
    @likes.each do |like|
      array.push(like.user_id)
    end
    return array.include?(user.id)
  end

  def count_likes(post)
    number = post.likes.count
    if number == 0
      return nil
    elsif number == 1
      return "1 like"
    else
      return number.to_s + " likes"
    end
  end

end
