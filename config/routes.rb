Rails.application.routes.draw do

  get 'posts/index'
  post 'posts/upload_post', as: 'upload_post'
  get 'p/:id', to: 'posts#view', as: :post
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  get 'lp/:id', to: 'likes#like_post', as: 'like'
  get 'up/:id', to: 'likes#unlike_post', as: 'unlike'

  post 'p/:id', to: 'posts#post_comment', as: 'post_comment'

  root to: "home#index"

end
